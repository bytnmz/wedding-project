const defaultTheme = require("tailwindcss/defaultTheme");

module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      colors: {
        gold: "#968a78",
        darkgold: "#70675A",
        themegreen: "#0ABAB5",
        darkgreen: "#088E8B",
        success: "#009B77",
        error: "#ef3340",
        link: "#0d6efd",
      },
      fontFamily: {
        sans: ["Inter", ...defaultTheme.fontFamily.sans],
        body: ["Montserrat", ...defaultTheme.fontFamily.sans],
        cursive: ["Carried Away", ...defaultTheme.fontFamily.sans],
      },
    },
  },
  plugins: [require("@tailwindcss/forms")],
};
