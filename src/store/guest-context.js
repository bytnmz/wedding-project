import { v4 as uuidv4 } from "uuid";
import axios from "axios";
import React, { useCallback, useEffect, useState } from "react";
import { useLocation } from "react-router-dom";

const GuestContext = React.createContext({
  guestData: {
    _id: null,
    isattending: "Yes",
    name: "",
    contact: "",
    email: "",
    address: "",
    dietaryrestrictions: {
      vegan: {
        value: false,
      },
      halal: {
        value: false,
      },
      nobeef: {
        value: false,
      },
      others: {
        enabled: false,
        value: "",
      },
    },
    requirements: "",
    parkingcoupon: "No",
    additionalGuests: [],
  },
  guestId: "",
  hasGuestId: false,
  fetchingError: false,
  updateSuccess: false,
  onAddNewGuest: () => {},
  onRemoveGuest: (guestId) => {},
  onMainGuestPropertyChange: (value, property) => {},
  onMainGuestDietaryRestrictionsChange: (value, name) => {},
  onMainGuestToggleOthers: () => {},
  onAdditionalGuestPropertyChange: (i, value, property) => {},
  onAdditionalGuestDietaryRestrictionsChange: (i, value, name) => {},
  onAdditionalGuestToggleOthers: (i) => {},
  onGuestIdChange: (i) => {},
  onUpdateGuestData: (status) => {},
});

export const GuestContextProvider = (props) => {
  const location = useLocation();

  const [guestId, setGuestId] = useState("");
  const [hasGuestId, setHasGuestId] = useState(false);
  const [fetchingError, setFetchingError] = useState(false);
  const [updateSuccess, setUpdateSuccess] = useState(false);
  const [guestData, setGuestData] = useState({
    _id: uuidv4(),
    isattending: "Yes",
    name: "",
    contact: "",
    email: "",
    address: "",
    dietaryrestrictions: {
      vegan: {
        value: false,
      },
      halal: {
        value: false,
      },
      nobeef: {
        value: false,
      },
      others: {
        enabled: false,
        value: "",
      },
    },
    requirements: "",
    parkingcoupon: "No",
    additionalGuests: [],
  });

  const getGuestData = useCallback(async (params) => {
    const data = {
      id: params.get("guestId") ? params.get("guestId") : params.get("guestid"),
    };

    try {
      const response = await axios({
        method: "post",
        url: `${process.env.REACT_APP_BACKEND_URL}guests/get`,
        data,
      });

      setGuestData((prevState) => {
        return {
          ...prevState,
          ...response.data.guest,
        };
      });

      setHasGuestId(true);
      setGuestId(data.id);
    } catch (error) {
      console.log(error);
      setFetchingError(true);
    }
  }, []);

  useEffect(() => {
    if (location.search) {
      const queryParams = new URLSearchParams(location.search);

      if (queryParams.has("guestId") || queryParams.has("guestid")) {
        getGuestData(queryParams);
      }
    }
  }, [location, getGuestData]);

  const addNewGuestHandler = (e) => {
    e.preventDefault();

    const newGuest = {
      id: uuidv4(),
      name: "",
      dietaryrestrictions: {
        vegan: {
          value: false,
        },
        halal: {
          value: false,
        },
        nobeef: {
          value: false,
        },
        others: {
          enabled: false,
          value: "",
        },
      },
    };

    const newAdditionalGuests = [...guestData.additionalGuests, newGuest];
    setGuestData((prevState) => {
      return {
        ...prevState,
        additionalGuests: newAdditionalGuests,
      };
    });
  };

  const removeGuestHandler = (guestId) => {
    const currentAdditionalGuestList = [...guestData.additionalGuests];
    const removedGuestList = currentAdditionalGuestList.filter((guest) => {
      if (guest._id) {
        return guest._id !== guestId;
      } else {
        return guest.id !== guestId;
      }
    });

    setGuestData((prevState) => {
      return {
        ...prevState,
        additionalGuests: removedGuestList,
      };
    });
  };

  const mainGuestPropertyChangeHandler = (value, property) => {
    const currentData = { ...guestData };
    currentData[property] = value;
    setGuestData(currentData);
  };

  const mainGuestDietaryRestrictionsChangeHandler = (value, name) => {
    const currentData = { ...guestData };
    currentData.dietaryrestrictions[name].value = value;
    setGuestData(currentData);
  };

  const mainGuestToggleOthersHandler = () => {
    const currentData = { ...guestData };
    currentData.dietaryrestrictions.others.enabled =
      !currentData.dietaryrestrictions.others.enabled;
    setGuestData(currentData);
  };

  const additionalGuestPropertyChangeHandler = (i, value, property) => {
    const currentAdditionalGuestList = [...guestData.additionalGuests];
    const targetGuest = { ...currentAdditionalGuestList[i] };
    targetGuest[property] = value;
    currentAdditionalGuestList[i] = targetGuest;

    setGuestData((prevState) => {
      return {
        ...prevState,
        additionalGuests: currentAdditionalGuestList,
      };
    });
  };

  const additionalGuestDietaryRestrictionsChangeHandler = (i, value, name) => {
    const currentAdditionalGuestList = [...guestData.additionalGuests];
    const targetGuest = { ...currentAdditionalGuestList[i] };
    targetGuest.dietaryrestrictions[name].value = value;
    currentAdditionalGuestList[i] = targetGuest;

    setGuestData((prevState) => {
      return {
        ...prevState,
        additionalGuests: currentAdditionalGuestList,
      };
    });
  };

  const additionalGuestToggleOthersHandler = (i) => {
    const currentAdditionalGuestList = [...guestData.additionalGuests];
    const targetGuest = { ...currentAdditionalGuestList[i] };
    targetGuest.dietaryrestrictions.others.enabled =
      !targetGuest.dietaryrestrictions.others.enabled;
    currentAdditionalGuestList[i] = targetGuest;

    setGuestData((prevState) => {
      return {
        ...prevState,
        additionalGuests: currentAdditionalGuestList,
      };
    });
  };

  const guestIdChange = (value) => {
    setGuestId(value);
  };

  const updateGuestDataHandler = (status) => {
    if (status === 200) {
      setUpdateSuccess(true);
    } else {
      setUpdateSuccess(false);
    }
  };

  return (
    <GuestContext.Provider
      value={{
        guestData: guestData,
        guestId: guestId,
        hasGuestId: hasGuestId,
        fetchingError: fetchingError,
        updateSuccess: updateSuccess,
        onAddNewGuest: addNewGuestHandler,
        onRemoveGuest: removeGuestHandler,
        onMainGuestPropertyChange: mainGuestPropertyChangeHandler,
        onMainGuestDietaryRestrictionsChange:
          mainGuestDietaryRestrictionsChangeHandler,
        onMainGuestToggleOthers: mainGuestToggleOthersHandler,
        onAdditionalGuestPropertyChange: additionalGuestPropertyChangeHandler,
        onAdditionalGuestDietaryRestrictionsChange:
          additionalGuestDietaryRestrictionsChangeHandler,
        onAdditionalGuestToggleOthers: additionalGuestToggleOthersHandler,
        onGuestIdChange: guestIdChange,
        onUpdateGuestData: updateGuestDataHandler,
      }}
    >
      {props.children}
    </GuestContext.Provider>
  );
};

export default GuestContext;
