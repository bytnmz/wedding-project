import React from "react";

import "./Suggestions.css";

function Suggestions(props) {
  const buttonText = (name) => {
    const index = name.toLowerCase().indexOf(props.nameInput.toLowerCase());
    const length = props.nameInput.length;

    const highlighted = name.slice(index, index + length);

    return <strong>{highlighted}</strong>;
  };
  return (
    <div className="suggestions-list">
      {props.suggestionsList.length > 0 && (
        <ul>
          {props.suggestionsList.map((person, index) => (
            <li key={index}>
              <button
                type="button"
                onMouseEnter={props.hoverHandler}
                onClick={props.suggestionClickHandler}
                value={person.name}
              >
                {person.name.slice(
                  0,
                  person.name
                    .toLowerCase()
                    .indexOf(props.nameInput.toLowerCase())
                )}
                {buttonText(person.name)}
                {person.name.slice(
                  person.name
                    .toLowerCase()
                    .indexOf(props.nameInput.toLowerCase()) +
                    props.nameInput.length
                )}
              </button>
            </li>
          ))}
        </ul>
      )}
    </div>
  );
}

export default Suggestions;
