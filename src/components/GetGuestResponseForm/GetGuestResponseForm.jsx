import React, { useEffect, useState, useCallback } from "react";
import axios from "axios";

import Input from "../../shared/Form/Input";
import SubmitButton from "./SubmitButton";
import InvitationFormHeader from "../InvitationFormHeader";
import Suggestions from "./Suggestions";

import "./GetGuestResponseForm.css";

function GetGuestResponseForm(props) {
  const [name, setName] = useState("");
  const [isTyping, setIsTyping] = useState(false);
  const [showSuggestions, setShowSuggestions] = useState(false);
  const [suggestions, setSuggestions] = useState([]);

  const nameChangeHandler = (event) => {
    setShowSuggestions(false);
    setName(event.target.value);
  };

  const keyUpHandler = (event) => {
    setIsTyping(true);
  };

  const suggestionClickHandler = (event) => {
    setIsTyping(false);
    setShowSuggestions(false);
    setName(event.target.value);
  };

  const getGuestData = useCallback(async () => {
    const data = { name };

    try {
      const response = await axios({
        method: "post",
        url: `${process.env.REACT_APP_BACKEND_URL}guests/get`,
        data,
      });

      setSuggestions(response.data);
    } catch (error) {
      console.log(error);
    }

    setShowSuggestions(true);
  }, [name]);

  useEffect(() => {
    if (isTyping && name.length > 1) {
      const timer = setTimeout(() => {
        getGuestData();
      }, 300);

      return () => {
        clearTimeout(timer);
      };
    }
  }, [name, isTyping, getGuestData]);

  return (
    <form className="get-guest-response-form" onSubmit={getGuestData}>
      <InvitationFormHeader></InvitationFormHeader>
      <div className="get-guest-response-form__body">
        <Input
          inputLabel="Name"
          inputName="name"
          placeholder="Enter your name"
          inputChangeHandler={nameChangeHandler}
          keyUpHandler={keyUpHandler}
          value={name}
          autoComplete="off"
          isRequired="true"
        >
          {showSuggestions && (
            <Suggestions
              suggestionsList={suggestions}
              suggestionClickHandler={suggestionClickHandler}
              nameInput={name}
            ></Suggestions>
          )}
        </Input>
        <footer className="get-guest-response-form__footer">
          <SubmitButton textLabel="Next"></SubmitButton>
        </footer>
      </div>
    </form>
  );
}

export default GetGuestResponseForm;
