import React from "react";
import { Fade } from "react-awesome-reveal";

const Footer = () => {
  return (
    <Fade duration="650" triggerOnce>
      <footer className="bg-gold py-6 md:py-8">
        <div className="container">
          <h2 className="text-center font-semibold text-white m-0 text-2xl lg:text-3xl mb-0 md:mb-1">
            See you at the wedding!
          </h2>
          <p className="text-center font-medium text-white m-0 block text-sm md:text-base">
            With love, Bayu & Wenjin
          </p>
        </div>
      </footer>
    </Fade>
  );
};

export default Footer;
