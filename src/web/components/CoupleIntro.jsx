import React from "react";
import HexagonImage from "./Hexagon";
import { Fade } from "react-awesome-reveal";

import "./CoupleIntro.css";

const CoupleIntro = () => {
  return (
    <section
      id="couple"
      className="couple-intro py-4 max-w-[1280px] mx-auto my-[64px] overflow-hidden"
    >
      <div className="container max-w-[960px]">
        <Fade direction="up" duration="650" triggerOnce>
          <h2 className="text-center text-2xl lg:text-3xl font-medium text-neutral-700 mb-4 md:mb-12">
            The Couple
          </h2>
        </Fade>
        <Fade direction="up" duration="650" triggerOnce>
          <div className="flex lg:items-center justify-start mb-2 md:mb-8">
            <HexagonImage className="hexagon-card--left" img="groom.webp" />
            <div className="hexagon-card__text mb-6 pt-6 md:pt-5 pb-0 md:pb-5 pl-6 md:pl-10 md:ml-10 md:border-l md:border-gold">
              <span className="font-light text-sm block mb-2 md:mb-4 text-neutral-800">
                Groom
              </span>
              <h3 className="font-['Montserrat'] font-medium text-xl lg:text-2xl mb-1 text-neutral-700">
                Bayu Tanmizi
              </h3>
              <p className="text-sm lg:text-base text-neutral-700 mb-2">
                Just look at what he's doing in the photo... Yeah, he's that guy
                who takes notice of and googles the random stuff. Seriously,
                what's so interesting about the flower stalks???
              </p>
              <p className="text-sm lg:text-base text-neutral-700 mb-2">
                That nonsensical guy who deliberately over-dramatises everything
                just for the fun of it when he's supposed to be the one solving
                the problems.
              </p>
              <p className="text-sm lg:text-base text-neutral-700">
                His wingman was his cute white bunny! Das right~ Das how he got
                her~ And his best cleaning weapon is the lint roller from Muji.
                #cuzThereIsTooMuchFurEverywhere
              </p>
            </div>
          </div>
        </Fade>
        <Fade direction="up" duration="650" triggerOnce>
          <div className="flex flex-row-reverse lg:items-center justify-end">
            <HexagonImage className="hexagon-card--right" img="bride.webp" />
            <div className="hexagon-card__text pt-6 md:pt-5 pb-0 md:pb-5 pr-6 md:pr-10 md:mr-10 md:border-r text-right md:border-gold">
              <span className="font-light text-sm block mb-4 md:mb-4 text-neutral-800">
                Bride
              </span>
              <h3 className="font-['Montserrat'] font-medium text-xl lg:text-2xl mb-1 text-neutral-700">
                Yu Wenjin
              </h3>
              <p className="text-sm lg:text-base text-neutral-700 mb-2">
                The typical curious and inquisitive Sagittarius who had more
                curiosity for a rabbit than the actual guy himself initially.
              </p>
              <p className="text-sm lg:text-base text-neutral-700 mb-2">
                Also, the one who (still) sleeps too much... So much that she
                ghosted him the whole afternoon only to wake up just in time in
                the evening to reply to a second date.
              </p>
              <p className="text-sm lg:text-base text-neutral-700">
                Was she relieved she woke up and got her Mr Right!
              </p>
              {/* <p className="text-sm lg:text-base text-neutral-700">
              That's how she met her Mr Right - naively out of curiosity for his
              cute bunny but as usual, she got distracted and eventually ended
              up admiring how caring and patient he was in taking care of his
              bunny.
            </p> */}
            </div>
          </div>
        </Fade>
      </div>
    </section>
  );
};

export default CoupleIntro;
