import React from "react";
import Countdown from "react-countdown";
import { Fade } from "react-awesome-reveal";

const renderer = ({ days, hours, minutes, seconds }) => {
  return (
    <div className="flex justify-center lg:justify-around bg-white/[.85] rounded max-w-[640px] mx-auto">
      <span className="flex flex-col items-center p-2 md:p-4 w-[80px] md:w-[100px]">
        <span className="font-semibold tracking-wider text-xl lg:text-4xl mb-1 text-gold">
          {days}
        </span>
        <span className="tracking-wider text-sm lg:text-base text-neutral-700">
          days
        </span>
      </span>
      <span className="flex flex-col items-center p-2 md:p-4 w-[80px] md:w-[100px]">
        <span className="font-semibold tracking-wider text-xl lg:text-4xl mb-1 text-gold">
          {hours}
        </span>
        <span className="tracking-wider text-sm lg:text-base text-neutral-700">
          hours
        </span>
      </span>
      <span className="flex flex-col items-center p-2 md:p-4 w-[80px] md:w-[100px]">
        <span className="font-semibold tracking-wider text-xl lg:text-4xl mb-1 text-gold">
          {minutes}
        </span>
        <span className="tracking-wider text-sm lg:text-base text-neutral-700">
          minutes
        </span>
      </span>
      <span className="flex flex-col items-center p-2 md:p-4 w-[80px] md:w-[100px]">
        <span className="font-semibold tracking-wider text-xl lg:text-4xl mb-1 text-gold">
          {seconds}
        </span>
        <span className="tracking-wider text-sm lg:text-base text-neutral-700">
          seconds
        </span>
      </span>
    </div>
  );
};

const IntroParagraph = () => {
  return (
    <section className="max-w-[800px] mx-auto my-[64px] text-center">
      <div className="container">
        <Fade direction="up" duration="650" triggerOnce cascade damping={0.15}>
          <h2 className="text-2xl lg:text-3xl font-medium text-neutral-700 mb-8 md:mb-12">
            The Dream Day
          </h2>
          <p className="text-sm lg:text-base font-light mb-4 md:mb-6 max-w-[600px] text-center ml-auto mr-auto">
            It's every couple's dream is to share their happiest moment of their
            life with all their loved ones!
          </p>
          <p className="text-sm lg:text-base font-light mb-6 md:mb-12 max-w-[600px] text-center ml-auto mr-auto">
            We're ecstatic to have all of you present on our special moment with
            us!
          </p>

          <p className="text-sm lg:text-base font-medium">We're only</p>
          <Countdown
            date={new Date("October 30, 2022 18:30:00")}
            renderer={renderer}
          />
          <p className="text-sm lg:text-base font-medium">away!</p>
        </Fade>
      </div>
    </section>
  );
};

export default IntroParagraph;
