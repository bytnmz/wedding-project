import React from "react";

import "./Hamburger.css";

const HamburgerIcon = () => {
  return (
    <div className="menu-icon">
      <div className="line line--1"></div>
      <div className="line line--2"></div>
      <div className="line line--3"></div>
    </div>
  );
};

export default HamburgerIcon;
