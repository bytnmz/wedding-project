import React from "react";
import { Fade } from "react-awesome-reveal";

const HorizontalRule = () => {
  return (
    <Fade direction="up" duration="650" triggerOnce>
      <div className="w-3/5 max-w-[400px] mx-auto my-20 md:my-24 relative">
        <hr className="bg-gold text-gold border-0 h-px" />
        <div className="absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 bg-white font-cursive text-gold text-xs md:text-sm font-semibold">
          <span className="inline-block -scale-x-100 translate-x-[10px] md:translate-x-3 rotate-[40deg]">
            ]
          </span>
          <span className="inline-block"> </span>
          <span className="inline-block -translate-x-[10px] md:-translate-x-3 rotate-[-40deg]">
            ]
          </span>
        </div>
      </div>
    </Fade>
  );
};

export default HorizontalRule;
