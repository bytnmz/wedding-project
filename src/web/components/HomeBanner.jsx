import React from "react";
import Slider from "react-slick";
import { Zoom } from "react-awesome-reveal";

import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import "./HomeBanner.css";

const HomeBanner = () => {
  const settings = {
    arrows: false,
    autoplay: true,
    autoplaySpeed: 7500,
    dots: false,
    fade: true,
    infinite: true,
    speed: 1500,
    slidesToShow: 1,
    slidesToScroll: 1,
  };

  return (
    <Zoom duration="1000" triggerOnce>
      <header id="home" className="banner-carousel">
        <Slider className="banner-carousel__carousel" {...settings}>
          <div className="banner-slide">
            <picture>
              <source
                media="(min-width:768px)"
                srcSet={`${process.env.REACT_APP_IMAGE_ASSET_URL}banner-d-1.webp`}
              />
              <img
                src={`${process.env.REACT_APP_IMAGE_ASSET_URL}banner-m-1.webp`}
                alt=""
              />
            </picture>
          </div>
          <div className="banner-slide">
            <picture>
              <source
                media="(min-width:768px)"
                srcSet={`${process.env.REACT_APP_IMAGE_ASSET_URL}banner-d-2.webp`}
              />
              <img
                src={`${process.env.REACT_APP_IMAGE_ASSET_URL}banner-m-2.webp`}
                alt=""
              />
            </picture>
          </div>
          <div className="banner-slide">
            <picture>
              <source
                media="(min-width:768px)"
                srcSet={`${process.env.REACT_APP_IMAGE_ASSET_URL}banner-d-3.webp`}
              />
              <img
                src={`${process.env.REACT_APP_IMAGE_ASSET_URL}banner-m-3.webp`}
                alt=""
              />
            </picture>
          </div>
          <div className="banner-slide">
            <picture>
              <source
                media="(min-width:768px)"
                srcSet={`${process.env.REACT_APP_IMAGE_ASSET_URL}banner-d-4.webp`}
              />
              <img
                src={`${process.env.REACT_APP_IMAGE_ASSET_URL}banner-m-4.webp`}
                alt=""
              />
            </picture>
          </div>
        </Slider>
        <div className="banner-carousel__frame"></div>
        <div className="banner-carousel__text">
          <h1>Bayu & Wenjin</h1>
          <p>Sunday, 30 October 2022</p>
          <small className="font-medium">
            The Glasshouse <span className="font-cursive font-semibold">@</span>{" "}
            Andaz
          </small>
        </div>
      </header>
    </Zoom>
  );
};

export default HomeBanner;
