import React, { useState } from "react";
import * as Scroll from "react-scroll";

import "./Menu.css";
import HamburgerIcon from "./Hamburger";

const Link = Scroll.Link;
// let Element = Scroll.Element;
// let Events = Scroll.Events;
// let scroll = Scroll.animateScroll;
// let scrollSpy = Scroll.scrollSpy;

const Menu = () => {
  const [isActive, setIsActive] = useState(false);

  const menuStateHandler = (e) => {
    e.preventDefault();
    e.stopPropagation();

    setIsActive((prevState) => !prevState);
  };

  return (
    <div className={`menu${isActive ? " active" : ""}`}>
      <button
        type="button"
        className="flex items-center bg-white px-5 text-darkgold py-2 rounded-full drop-shadow-lg"
        onClick={menuStateHandler}
      >
        <HamburgerIcon />
        <span className="block font-medium ml-2 w-[48px] text-center">
          {isActive ? "Close" : "Menu"}
        </span>
      </button>
      <nav className="bg-white drop-shadow-lg px-8 py-8 rounded absolute w-[200px] bottom-[calc(100%+10px)] md:bottom-[auto] md:top-[calc(100%+10px)] right-0">
        <Link
          href="#"
          className="block mb-4"
          activeClass="active"
          to="home"
          spy={true}
          smooth={true}
          offset={-50}
          duration={500}
        >
          Home
        </Link>
        <Link
          href="#"
          className="block mb-4"
          activeClass="active"
          to="couple"
          spy={true}
          smooth={true}
          offset={-50}
          duration={500}
        >
          The Couple
        </Link>
        <Link
          href="#"
          className="block mb-4"
          activeClass="active"
          to="story"
          spy={true}
          smooth={true}
          offset={-50}
          duration={500}
        >
          Their Story
        </Link>
        <Link
          href="#"
          className="block mb-4"
          activeClass="active"
          to="schedule"
          spy={true}
          smooth={true}
          offset={-50}
          duration={500}
        >
          Schedule
        </Link>
        <Link
          href="#"
          className="block"
          activeClass="active"
          to="directions"
          spy={true}
          smooth={true}
          offset={-50}
          duration={500}
        >
          Getting There
        </Link>
      </nav>
    </div>
  );
};

export default Menu;
