import React from "react";
import { Fade } from "react-awesome-reveal";

const EventSchedule = () => {
  return (
    <section
      id="schedule"
      className="max-w-[1280px] mx-auto my-[64px] text-center"
    >
      <div className="container max-w-[960px]">
        <Fade direction="up" duration="650" triggerOnce>
          <h2 className="text-2xl lg:text-3xl font-medium text-neutral-700 mb-4 md:mb-12">
            Event Schedule
          </h2>
        </Fade>
        <Fade direction="up" duration="650" triggerOnce cascade damping={0.15}>
          <ul className="text-sm lg:text-base text-left">
            <li className="flex justify-between">
              <time className="text-right pr-6 lg:pr-12 basis-1/4 md:basis-1/3 grow-0 shrink-0 text-xs lg:text-sm text-neutral-700">
                1830 - 1900
              </time>
              <div className="relative basis-3/4 md:basis-2/3 grow-0 shrink-0 border-l-2 border-solid border-gold pb-6 lg:pb-8 pl-5 lg:pl-12 before:content-[''] before:absolute before:w-[12px] before:h-[12px] before:rounded-lg before:block before:top-1 before:left-[-7px] before:bg-white before:border-darkgold before:border before:border-solid">
                <h3 className="font-['Montserrat'] text-sm lg:text-base font-semibold mb-1">
                  Guest arrival and seating
                </h3>
                <span className="text-xs lg:text-sm block mb-2 md:mb-4 text-neutral-800">
                  The Glasshouse Foyer
                </span>
              </div>
            </li>
            <li className="flex">
              <time className="text-right pr-6 lg:pr-12 basis-1/4 md:basis-1/3 grow-0 shrink-0 text-xs lg:text-sm text-neutral-700">
                1900 - 2200
              </time>
              <div className="relative basis-3/4 md:basis-2/3 grow-0 shrink-0 border-l-2 border-solid border-gold pl-5 lg:pl-12 before:content-[''] before:absolute before:w-[12px] before:h-[12px] before:rounded-lg before:block before:top-1 before:left-[-7px] before:bg-white before:border-darkgold before:border before:border-solid">
                <h3 className="font-['Montserrat'] text-sm lg:text-base font-semibold mb-1">
                  Wedding Banquet
                </h3>
                <span className="text-xs lg:text-sm block mb-2 md:mb-4 text-neutral-800">
                  The Glasshouse
                </span>
                <p className="text-sm lg:text-base">
                  8-course Chinese menu!
                  <br></br>
                  Yum! :P
                </p>
              </div>
            </li>
          </ul>
        </Fade>
      </div>
    </section>
  );
};

export default EventSchedule;
