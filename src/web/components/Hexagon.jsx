import React from "react";

import "./Hexagon.css";

const HexagonImage = ({ className, img }) => {
  const style = {
    backgroundImage: "url(" + process.env.REACT_APP_IMAGE_ASSET_URL + img + ")",
  };

  return (
    <div className={`hexagon-card ${className}`}>
      <svg version="1.0" viewBox="0 0 256 256" className="hexagonsvg1">
        <path d="m 197.92817,172.13075 -70.10232,40.47359 -70.102321,-40.4736 0,-80.947185 70.102321,-40.473594 70.10232,40.473595 z" />
      </svg>
      <svg version="1.0" viewBox="0 0 256 256" className="hexagonsvg2">
        <path d="m 197.92817,172.13075 -70.10232,40.47359 -70.102321,-40.4736 0,-80.947185 70.102321,-40.473594 70.10232,40.473595 z" />
      </svg>
      {/* <svg version="1.0" viewBox="0 0 256 256" id="hexagonsvg3">
        <path d="m 197.92817,172.13075 -70.10232,40.47359 -70.102321,-40.4736 0,-80.947185 70.102321,-40.473594 70.10232,40.473595 z" />
      </svg> */}

      <div className={`hexagon`}>
        <div className="hexagon-inner1">
          <div className="hexagon-inner2" style={style}></div>
        </div>
      </div>
    </div>
  );
};

export default HexagonImage;
