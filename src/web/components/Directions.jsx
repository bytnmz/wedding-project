import React from "react";
import { Fade } from "react-awesome-reveal";

import "./Directions.css";

const Directions = () => {
  return (
    <section id="directions" className="max-w-[1280px] mx-auto my-[64px] pb-12">
      <div className="container max-w-[960px]">
        <Fade direction="up" duration="650" cascade triggerOnce>
          <h2 className="text-center text-2xl lg:text-3xl font-medium text-neutral-700 mb-4 md:mb-12">
            Map & Directions
          </h2>
          <div className="relative">
            <div className="mb-6 mx-[-10px] md:mx-0 md:mb-0 md:w-1/2 md:shadow-xl md:absolute md:right-0 md:bottom-0 md:translate-y-[100px]">
              <img
                src={`${process.env.REACT_APP_IMAGE_ASSET_URL}andaz-map.webp`}
                alt="Map of Andaz"
                loading="lazy"
              />
            </div>
            <div className="border-2 border-solid border-gold md:w-3/4 lg:w-4/5 p-6 flex flex-col gap-5 md:mb-[100px]">
              <div className="md:w-3/5 md:hidden">
                <h3 className="font-['Montserrat'] font-semibold mb-1">
                  Andaz Singapoore
                </h3>
                <p className="text-sm lg:text-base">
                  5 Fraser Street, Singapore 189354
                </p>
                <small className="italic">
                  Andaz Singapore is located within DUO.
                </small>
              </div>
              <div className="">
                <h3 className="font-['Montserrat'] font-semibold mb-1">
                  By Train:
                </h3>
                <p className="text-sm lg:text-base">
                  Take Downtown or East West Line to the Bugis MRT station
                  (DT14/EW12), which offers direct underpass access to Andaz
                  Singapore via DUO Galleria.
                </p>
              </div>
              <div className="custom-width">
                <h3 className="font-['Montserrat'] font-semibold mb-1">
                  By Bus:
                </h3>
                <p className="text-sm lg:text-base">
                  The nearest bus stop Parkview Square (01139) is a 2-minute
                  walk to Andaz Singapore.
                </p>
              </div>
              <div className="md:w-3/5">
                <h3 className="font-['Montserrat'] font-semibold mb-1">
                  By Car:
                </h3>
                <ul className="text-sm lg:text-base pl-6">
                  <li className="list-disc">
                    Underground parking is available at DUO Galleria
                  </li>
                  <li className="list-disc">
                    Hotel entrance is accessible via Level 2 ramp
                  </li>
                </ul>
              </div>
              <div className="md:w-3/5 hidden md:block">
                <h3 className="font-['Montserrat'] font-semibold mb-1">
                  Andaz Singapoore
                </h3>
                <p className="text-sm lg:text-base">
                  5 Fraser Street, Singapore 189354
                </p>
                <small className="italic">
                  Andaz Singapore is located within DUO.
                </small>
              </div>
              <div className="md:w-3/5">
                <a
                  className="underline text-darkgold font-semibold text-sm"
                  href="https://www.google.com/maps/dir//Andaz+Singapore+-+a+Concept+by+Hyatt+5+Fraser+St+Singapore+189354/@1.2992646,103.8581702,16z/data=!4m8!4m7!1m0!1m5!1m1!1s0x31da19b021e9ce07:0x6f6afbcfba9af1c1!2m2!1d103.8581702!2d1.2992646"
                  target="_blank"
                  rel="noreferrer"
                >
                  Directions
                </a>
              </div>
            </div>
          </div>
        </Fade>
      </div>
    </section>
  );
};

export default Directions;
