import React from "react";
import { Fade } from "react-awesome-reveal";

const OurStory = () => {
  return (
    <section
      id="story"
      className="max-w-[1280px] mx-auto my-[64px] text-center"
    >
      <div className="container max-w-[800px]">
        <Fade direction="up" duration="650" triggerOnce cascade damping={0.15}>
          <h2 className="text-2xl md:text-3xl font-medium text-neutral-700 mb-4 md:mb-12">
            Their Story
          </h2>
          <p className="text-sm lg:text-base text-neutral-700 px-3 md:px-0 mb-2">
            Bayu and Wenjin got close with the help of a cute little white bunny
            cupid! Wenjin had never seen such a cute bunny in her life and he
            quickly became a reason for both Bayu and Wenjin to meet!
          </p>
          <p className="text-sm lg:text-base text-neutral-700 px-3 md:px-0">
            Before they knew it, their favorite activity became grocery shopping
            together during the dreaded pandemic period and also the late night
            suppers they deeply regret eating as the big day draws near...
          </p>
          <small className="block italic font-semibold text-xs mt-4 break-words">
            #theRabbitWasABaitAndItWorked
            #whereElseButTheSupermarketDuringLockdown
            #damnThoseLateNightCalories
          </small>
        </Fade>
      </div>
    </section>
  );
};

export default OurStory;
