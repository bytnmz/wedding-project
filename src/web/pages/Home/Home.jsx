import React from "react";
import HomeBanner from "../../components/HomeBanner";
import CoupleIntro from "../../components/CoupleIntro";
import Directions from "../../components/Directions";
import IntroParagraph from "../../components/IntroParagraph";
import OurStory from "../../components/OurStory";
import EventSchedule from "../../components/EventSchedule";
import Footer from "../../components/Footer";
import HorizontalRule from "../../components/HorizontalRule";
import Menu from "../../components/Menu";

const Home = () => {
  return (
    <React.Fragment>
      <Menu />
      <HomeBanner />
      <IntroParagraph />
      <HorizontalRule />
      <CoupleIntro />
      <HorizontalRule />
      <OurStory />
      <HorizontalRule />
      <EventSchedule />
      <HorizontalRule />
      <Directions />
      <Footer />
    </React.Fragment>
  );
};

export default Home;
