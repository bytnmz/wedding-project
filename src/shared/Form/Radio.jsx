import React from "react";

import "./Radio.css";

function Radio(props) {
  return (
    <div
      className={`custom-radio${props.inline ? " custom-radio--inline" : ""}`}
    >
      <input
        type="radio"
        name={props.radioName}
        id={props.radioID}
        onChange={props.changeHandler}
        value={props.radioValue}
        checked={props.isChecked}
      ></input>
      <label className="text-sm text-gray-800" htmlFor={props.radioID}>
        {props.radioLabel}
      </label>
    </div>
  );
}

export default Radio;
