import React from "react";

function SubmitButton(props) {
  return (
    <button
      className={
        props.className
          ? "flex justify-center items-center rounded bg-themegreen px-4 py-2 text-white text-sm border border-solid border-themegreen hover:bg-darkgreen focus:shadow-[0_0_0_4px_rgba(10,186,181,0.5)] transition-all " +
            props.className
          : "flex justify-center items-center rounded bg-themegreen px-4 py-2 text-white text-sm border border-solid border-themegreen hover:bg-darkgreen focus:shadow-[0_0_0_4px_rgba(10,186,181,0.5)] transition-all"
      }
      type="submit"
    >
      {props.textLabel ? props.textLabel : "Submit"}
    </button>
  );
}

export default SubmitButton;
