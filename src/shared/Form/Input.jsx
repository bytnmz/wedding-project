import React from "react";
import FormLabel from "./Label";

function Input(props) {
  return (
    <div className="mb-4 last:m-0">
      <FormLabel formFor={props.inputName}>
        {props.inputLabel}{" "}
        {props.isRequired && <span className="text-error">*</span>}
      </FormLabel>
      {props.isRequired ? (
        <input
          className="focus:ring-themegreen focus:border-themegreen block w-full text-gray-800 text-sm border-gray-300 rounded"
          type={props.inputType ? props.inputType : "text"}
          name={props.inputName}
          id={props.inputName}
          value={props.value}
          onChange={props.inputChangeHandler}
          onKeyUp={props.keyUpHandler}
          placeholder={props.placeholder}
          aria-describedby={props.describeById}
          autoComplete={props.autoComplete}
          required
        />
      ) : (
        <input
          className="focus:ring-themegreen focus:border-themegreen block w-full text-gray-800 text-sm border-gray-300 rounded"
          type={props.inputType ? props.inputType : "text"}
          name={props.inputName}
          id={props.inputName}
          value={props.value}
          onChange={props.inputChangeHandler}
          onKeyUp={props.keyUpHandler}
          placeholder={props.placeholder}
          autoComplete={props.autoComplete}
          aria-describedby={props.describeById}
        />
      )}
      {props.describeByText && (
        <small className="mt-2 text-xs text-gray-600" id={props.describeById}>
          {props.describeByText}
        </small>
      )}
      {props.children}
    </div>
  );
}

export default Input;
