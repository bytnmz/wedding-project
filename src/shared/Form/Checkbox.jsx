import React from "react";

import "./Checkbox.css";

function Checkbox(props) {
  return (
    <div
      className={`custom-checkbox${
        props.inline ? " custom-checkbox--inline" : ""
      }`}
    >
      <input
        type="checkbox"
        name={props.checkboxName}
        id={props.checkboxID}
        onChange={props.changeHandler}
        value={props.checkboxLabel}
        checked={props.isChecked}
      ></input>
      <label className="text-sm text-gray-800" htmlFor={props.checkboxID}>
        {props.checkboxLabel}
      </label>
    </div>
  );
}

export default Checkbox;
