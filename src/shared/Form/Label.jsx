import React from "react";

const FormLabel = (props) => {
  return (
    <label className="block text-sm text-gray-800 mb-2" htmlFor={props.formFor}>
      {props.children}
    </label>
  );
};

export default FormLabel;
