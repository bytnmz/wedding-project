import React, { Fragment, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Dialog, Transition } from "@headlessui/react";
import axios from "axios";
import Input from "../../shared/Form/Input";
import { uiActions } from "../../store/ui-slice";

const AddGuestModal = (props) => {
  const token = useSelector((state) => state.login.token);
  const dispatch = useDispatch();

  const [data, setData] = useState({
    isattending: "No",
    name: "",
    contact: "",
    email: "",
    address: "",
    dietaryrestrictions: {
      vegan: {
        value: false,
      },
      halal: {
        value: false,
      },
      nobeef: {
        value: false,
      },
      others: {
        enabled: false,
        value: "",
      },
    },
    requirements: "",
    parkingcoupon: "No",
    additionalGuests: [],
  });

  const inputChangeHandler = (e) => {
    const currentData = { ...data };
    currentData[e.target.name] = e.target.value;
    setData(currentData);
  };

  const addGuest = async (e) => {
    e.preventDefault();

    try {
      await axios({
        method: "post",
        url: `${process.env.REACT_APP_BACKEND_URL}guests/add`,
        headers: {
          Authorization: `Bearer ${token}`,
        },
        data,
      });

      props.onFetch();
      props.onClose();

      dispatch(
        uiActions.setNotification({
          title: "Successfully saved!",
          message: `Guest ${data.name} has been added to the guest list.`,
          success: true,
          show: true,
        })
      );
    } catch (error) {
      console.log(error);

      props.onClose();

      dispatch(
        uiActions.setNotification({
          title: "Failed!",
          message: `Unable to add ${data.name} to the guest list.`,
          success: false,
          show: true,
        })
      );
    }
  };

  return (
    <React.Fragment>
      <Transition.Root show={props.showModal} as={Fragment}>
        <Dialog as="div" className="relative z-10" onClose={props.onClose}>
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <div className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" />
          </Transition.Child>

          <div className="fixed z-10 inset-0 overflow-y-auto">
            <div className="flex items-end sm:items-center justify-center min-h-full p-4 text-center sm:p-0">
              <Transition.Child
                as={Fragment}
                enter="ease-out duration-300"
                enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                enterTo="opacity-100 translate-y-0 sm:scale-100"
                leave="ease-in duration-200"
                leaveFrom="opacity-100 translate-y-0 sm:scale-100"
                leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
              >
                <Dialog.Panel
                  as="form"
                  className="relative bg-white rounded-lg px-4 pt-5 pb-4 text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:max-w-sm sm:w-full sm:p-6"
                  onSubmit={addGuest}
                >
                  <div className="mt-3 text-center sm:mt-5">
                    <Dialog.Title
                      as="h2"
                      className="text-lg leading-6 font-medium text-gray-900"
                    >
                      Add a new guest
                    </Dialog.Title>
                    <div className="mt-4 text-left">
                      <Input
                        inputLabel="Guest Name"
                        inputName="name"
                        placeholder="Enter guest's name"
                        inputChangeHandler={inputChangeHandler}
                        autoComplete="off"
                        isRequired="true"
                      ></Input>
                      <Input
                        inputLabel="Contact Number"
                        inputName="contact"
                        inputType="tel"
                        inputChangeHandler={inputChangeHandler}
                        placeholder="Enter guest's contact number"
                        autoComplete="off"
                      ></Input>
                    </div>
                  </div>
                  <div className="mt-5 sm:mt-6 text-center">
                    <button
                      type="submit"
                      className="inline-flex justify-center w-full rounded-md border border-transparent shadow-sm px-4 py-2 bg-themegreen text-base font-medium text-white hover:bg-darkgreen focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-themegreen sm:text-sm transition-all"
                    >
                      Add guest
                    </button>
                    <button
                      type="button"
                      className="mt-3 text-base font-medium text-gray-700 hover:underline focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-themegreen sm:text-sm"
                      onClick={props.onClose}
                    >
                      Cancel
                    </button>
                  </div>
                </Dialog.Panel>
              </Transition.Child>
            </div>
          </div>
        </Dialog>
      </Transition.Root>
    </React.Fragment>
  );
};

export default AddGuestModal;
