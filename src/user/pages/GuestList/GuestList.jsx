import React, { useCallback, useEffect, useState } from "react";
import axios from "axios";
import { useDispatch, useSelector } from "react-redux";
import { dashboardActions } from "../../../store/dashboard-slice";
import { ClipboardDocumentIcon } from "@heroicons/react/24/outline";
import { UserMinusIcon } from "@heroicons/react/24/solid";
import AddGuestModal from "../../components/AddGuestModal";
import DeleteGuestModal from "../../components/DeleteGuestModal";
import GuestFilters from "./GuestFilters";
import { uiActions } from "../../../store/ui-slice";

const GuestList = (props) => {
  const [guestToRemove, setGuestToRemove] = useState("");
  const [showAddGuest, setShowAddGuest] = useState(false);
  const [showDeleteGuest, setShowDeleteGuest] = useState(false);
  const [fetchGuestList, setFetchGuestList] = useState(true);
  const token = useSelector((state) => state.login.token);
  const guestList = useSelector((state) => state.dashboard.guestList);
  const dispatch = useDispatch();

  const getAttendingGroups = (guestList) => {
    return guestList.filter((guest) => guest.isattending === "Yes").length;
  };

  const getTotalAttendingGuests = (guestList) => {
    const attendingGuests = guestList.filter(
      (guest) => guest.isattending === "Yes"
    );

    let total = 0;
    for (let i = 0; i < attendingGuests.length; i++) {
      total++;

      if (attendingGuests[i].additionalGuests.length > 0) {
        total += attendingGuests[i].additionalGuests.length;
      }
    }

    return total;
  };

  const attendingClasses = useCallback((attending) => {
    let bgColor = "bg-themegreen";

    if (attending === "No") {
      bgColor = "bg-error";
    }

    return `inline-flex rounded-full px-2 text-xs font-semibold leading-5 text-white ${bgColor}`;
  }, []);

  const getAllGuests = useCallback(async () => {
    try {
      const response = await axios({
        method: "get",
        url: `${process.env.REACT_APP_BACKEND_URL}guests/getAll`,
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });

      dispatch(
        dashboardActions.setGuest({
          guestList: response.data.guestList,
        })
      );
    } catch (error) {
      console.log(error);
    }
  }, [dispatch, token]);

  useEffect(() => {
    if (fetchGuestList) {
      getAllGuests();
      setFetchGuestList(false);
    }
  }, [getAllGuests, fetchGuestList]);

  const showAddGuestModal = (e) => {
    e.preventDefault();

    setShowAddGuest(true);
  };

  const closeAddGuestModal = (e) => {
    setShowAddGuest(false);
  };

  const showDeleteGuestModal = (e) => {
    e.preventDefault();

    setGuestToRemove(e.target.value);
    setShowDeleteGuest(true);
  };

  const closeDeleteGuestModal = (e) => {
    setShowDeleteGuest(false);
  };

  const fetchList = () => {
    setFetchGuestList(true);
  };

  const copyHandler = (e) => {
    e.preventDefault();

    const copied = `${window.location.protocol}//${window.location.host}/rsvp?guestId=${e.target.value}`;

    navigator.clipboard.writeText(copied);

    dispatch(
      uiActions.setNotification({
        title: "Copied!",
        message: `Copied ${copied} to your clipboard.`,
        success: true,
        show: true,
      })
    );
  };

  return (
    <React.Fragment>
      <AddGuestModal
        onFetch={fetchList}
        showModal={showAddGuest}
        onClose={closeAddGuestModal}
      />
      <DeleteGuestModal
        guest={guestToRemove}
        onFetch={fetchList}
        showModal={showDeleteGuest}
        onClose={closeDeleteGuestModal}
      />
      <section>
        <div className="sm:flex sm:items-center">
          <div className="sm:flex-auto">
            <h1 className="font-body text-3xl font-semibold text-gray-800">
              Guests
            </h1>
            <p className="mt-2 text-sm text-gray-700">
              A list of all the guests who are invited to the wedding.
            </p>
          </div>
          <div className="mt-4 sm:mt-0 sm:ml-16 sm:flex-none">
            <button
              type="button"
              className="inline-flex items-center justify-center rounded-md border border-transparent bg-themegreen px-4 py-2 text-sm font-medium text-white shadow-sm hover:bg-themegreen focus:outline-none focus:ring-2 focus:ring-themegreen focus:ring-offset-2 sm:w-auto"
              onClick={showAddGuestModal}
            >
              Add guest
            </button>
          </div>
        </div>
        <GuestFilters />
        <div className="mt-4">
          <span>
            Attending guests groups: {getAttendingGroups(guestList)} /{" "}
            {guestList.length}
          </span>
          <br></br>
          <span>
            Total attending guests: {getTotalAttendingGuests(guestList)}
          </span>
        </div>
        <div className="mt-8 overflow-hidden shadow ring-1 ring-black ring-opacity-5 rounded-lg">
          <table className="min-w-full divide-y divide-gray-300">
            <thead className="bg-slate-100">
              <tr>
                <th
                  scope="col"
                  className="py-3 pl-4 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-4"
                >
                  Name
                </th>
                <th
                  scope="col"
                  className="hidden px-3 py-3 text-left text-sm font-semibold text-gray-900 lg:table-cell"
                >
                  Contact Details
                </th>
                <th
                  scope="col"
                  className="hidden px-3 py-3 text-left text-sm font-semibold text-gray-900 lg:table-cell"
                >
                  Other Details
                </th>
                <th
                  scope="col"
                  className="hidden px-3 py-3 text-left text-sm font-semibold text-gray-900 lg:table-cell"
                >
                  Dietary Restrictions
                </th>
                <th
                  scope="col"
                  className="px-3 py-3 text-left text-sm font-semibold text-gray-900"
                >
                  Attending
                </th>
                <th scope="col" className="relative py-3 pl-3 pr-4 sm:pr-6">
                  <span className="sr-only">Actions</span>
                </th>
              </tr>
            </thead>
            <tbody className="divide-y divide-gray-200 bg-white">
              {guestList.map((guest, index) => (
                <React.Fragment key={guest._id}>
                  <tr className={index % 2 === 0 ? undefined : "bg-slate-100"}>
                    <td className="w-full max-w-0 py-3 pl-4 pr-3 text-sm font-bold text-gray-900 sm:w-auto sm:max-w-none sm:pl-4 align-top">
                      {guest.name}
                      <dl className="font-normal lg:hidden">
                        <dt className="sr-only">Contact Details</dt>
                        <dd className="mt-1 truncate text-gray-500">
                          {!!guest.contact && (
                            <span className="block">
                              <strong className="block">Contact:</strong>{" "}
                              {guest.contact}
                            </span>
                          )}
                          {!!guest.email && (
                            <span className="block">
                              <strong className="block">Email:</strong>{" "}
                              {guest.email}
                            </span>
                          )}
                        </dd>
                        <dt className="sr-only">Other Details</dt>
                        <dd className="mt-1 truncate text-gray-500">
                          {!!guest.address && (
                            <span className="block">
                              <strong className="block">Address:</strong>{" "}
                              {guest.address}
                            </span>
                          )}
                          {!!guest.requirements && (
                            <span className="block">
                              <strong className="block">
                                Special requirements:
                              </strong>{" "}
                              {guest.requirements}
                            </span>
                          )}
                          {!!guest.parkingcoupon === "Yes" && (
                            <span className="block">
                              <strong>Parking coupon:</strong>{" "}
                              {guest.parkingcoupon}
                            </span>
                          )}
                        </dd>
                        {(guest.dietaryrestrictions.halal.value ||
                          guest.dietaryrestrictions.nobeef.value ||
                          guest.dietaryrestrictions.vegan.value ||
                          guest.dietaryrestrictions.others.value) && (
                          <dt className="mt-1 text-gray-500 font-bold">
                            Dietary Restrictions:
                          </dt>
                        )}
                        <dd className="mt-1 text-gray-500">
                          {guest.dietaryrestrictions.halal.value && (
                            <span className="inline-flex rounded-full px-2 text-xs font-semibold leading-5 text-white bg-green-600 mr-2 mb-2">
                              Halal
                            </span>
                          )}
                          {guest.dietaryrestrictions.nobeef.value && (
                            <span className="inline-flex rounded-full px-2 text-xs font-semibold leading-5 text-white bg-red-500 mr-2 mb-2">
                              No beef
                            </span>
                          )}
                          {guest.dietaryrestrictions.vegan.value && (
                            <span className="inline-flex rounded-full px-2 text-xs font-semibold leading-5 text-white bg-lime-500 mr-2 mb-2">
                              Vegan
                            </span>
                          )}
                          {!!guest.dietaryrestrictions.others.value && (
                            <span className="inline-flex rounded-full px-2 text-xs font-semibold leading-5 text-white bg-blue-600 mr-2 mb-2">
                              {guest.dietaryrestrictions.others.value}
                            </span>
                          )}
                        </dd>
                      </dl>
                    </td>
                    <td className="hidden px-3 py-3 text-sm text-gray-500 lg:table-cell align-top space-y-2">
                      {!!guest.contact && (
                        <span className="block">
                          <strong className="block">Contact:</strong>{" "}
                          {guest.contact}
                        </span>
                      )}
                      {!!guest.email && (
                        <span className="block">
                          <strong className="block">Email:</strong>{" "}
                          {guest.email}
                        </span>
                      )}
                    </td>
                    <td className="hidden px-3 py-3 text-sm text-gray-500 lg:table-cell align-top space-y-2">
                      {!!guest.address && (
                        <span className="block">
                          <strong className="block">Address:</strong>{" "}
                          {guest.address}
                        </span>
                      )}
                      {!!guest.requirements && (
                        <span className="block">
                          <strong className="block">
                            Special requirements:
                          </strong>{" "}
                          {guest.requirements}
                        </span>
                      )}
                      {!!guest.parkingcoupon === "Yes" && (
                        <span className="block">
                          <strong>Parking coupon:</strong> {guest.parkingcoupon}
                        </span>
                      )}
                    </td>
                    <td className="hidden px-3 py-3 text-sm text-gray-500 lg:table-cell align-top whitespace-nowrap">
                      {guest.dietaryrestrictions.halal.value && (
                        <div>
                          <span className="inline-flex rounded-full px-2 text-xs font-semibold leading-5 text-white bg-green-600 mr-2 mb-2">
                            Halal
                          </span>
                        </div>
                      )}
                      {guest.dietaryrestrictions.nobeef.value && (
                        <div>
                          <span className="inline-flex rounded-full px-2 text-xs font-semibold leading-5 text-white bg-red-500 mr-2 mb-2">
                            No beef
                          </span>
                        </div>
                      )}
                      {guest.dietaryrestrictions.vegan.value && (
                        <div>
                          <span className="inline-flex rounded-full px-2 text-xs font-semibold leading-5 text-white bg-lime-500 mr-2 mb-2">
                            Vegan
                          </span>
                        </div>
                      )}
                      {!!guest.dietaryrestrictions.others.value && (
                        <div>
                          <span className="inline-flex rounded-full px-2 text-xs font-semibold leading-5 text-white bg-blue-600 mr-2 mb-2">
                            {guest.dietaryrestrictions.others.value}
                          </span>
                        </div>
                      )}
                    </td>
                    <td className="px-3 py-3 text-sm align-top">
                      <span className={attendingClasses(guest.isattending)}>
                        {guest.isattending}
                      </span>
                    </td>
                    <td className="py-3 pl-3 pr-4 text-right text-sm font-medium sm:pr-6 align-top">
                      <button
                        className="flex justify-center items-center text-themegreen mb-2 hover:underline"
                        value={guest._id}
                        onClick={copyHandler}
                      >
                        <ClipboardDocumentIcon className="h-5 w-5" />
                        Copy
                        <span className="sr-only">
                          , {guest.name} invite link
                        </span>
                      </button>

                      <button
                        className="flex justify-center items-center text-error mb-2 hover:underline"
                        onClick={showDeleteGuestModal}
                        value={`${guest._id},${guest.name}`}
                      >
                        <UserMinusIcon className="h-5 w-5" />
                        Remove
                        <span className="sr-only">, {guest.name}</span>
                      </button>
                      {/* <button className="flex justify-center items-center text-themegreen hover:underline">
                      <PencilAltIcon className="h-5 w-5" />
                      Edit<span className="sr-only">, {guest.name}</span>
                    </button> */}
                    </td>
                  </tr>
                  {!!guest.additionalGuests &&
                    guest.additionalGuests.map((additionalguest) => (
                      <tr
                        key={additionalguest._id}
                        className={index % 2 === 0 ? undefined : "bg-slate-100"}
                      >
                        <td className="w-full max-w-0 py-3 pl-4 pr-3 text-sm font-bold text-gray-900 sm:w-auto sm:max-w-none sm:pl-4 align-top">
                          {additionalguest.name}
                          <dl className="font-normal lg:hidden">
                            {(additionalguest.dietaryrestrictions.halal.value ||
                              additionalguest.dietaryrestrictions.nobeef
                                .value ||
                              additionalguest.dietaryrestrictions.vegan.value ||
                              additionalguest.dietaryrestrictions.others
                                .value) && (
                              <dt className="lg:hidden mt-1 text-gray-500 font-bold">
                                Dietary Restrictions:
                              </dt>
                            )}
                            <dd className="mt-1 text-gray-500 lg:hidden">
                              {additionalguest.dietaryrestrictions.halal
                                .value && (
                                <span className="inline-flex rounded-full px-2 text-xs font-semibold leading-5 text-white bg-green-600 mr-2 mb-2">
                                  Halal
                                </span>
                              )}
                              {additionalguest.dietaryrestrictions.nobeef
                                .value && (
                                <span className="inline-flex rounded-full px-2 text-xs font-semibold leading-5 text-white bg-red-500 mr-2 mb-2">
                                  No beef
                                </span>
                              )}
                              {additionalguest.dietaryrestrictions.vegan
                                .value && (
                                <span className="inline-flex rounded-full px-2 text-xs font-semibold leading-5 text-white bg-lime-500 mr-2 mb-2">
                                  Vegan
                                </span>
                              )}
                              {!!additionalguest.dietaryrestrictions.others
                                .value && (
                                <span className="inline-flex rounded-full px-2 text-xs font-semibold leading-5 text-white bg-blue-600 mr-2 mb-2">
                                  {
                                    additionalguest.dietaryrestrictions.others
                                      .value
                                  }
                                </span>
                              )}
                            </dd>
                          </dl>
                        </td>
                        <td className="hidden px-3 py-3 text-sm text-gray-500 align-top lg:table-cell"></td>
                        <td className="hidden px-3 py-3 text-sm text-gray-500 align-top lg:table-cell"></td>
                        <td className="hidden px-3 py-3 text-sm text-gray-500 align-top lg:table-cell whitespace-nowrap  ">
                          {additionalguest.dietaryrestrictions.halal.value && (
                            <div>
                              <span className="inline-flex rounded-full px-2 text-xs font-semibold leading-5 text-white bg-green-600 mr-2 mb-2">
                                Halal
                              </span>
                            </div>
                          )}
                          {additionalguest.dietaryrestrictions.nobeef.value && (
                            <div>
                              <span className="inline-flex rounded-full px-2 text-xs font-semibold leading-5 text-white bg-red-500 mr-2 mb-2">
                                No beef
                              </span>
                            </div>
                          )}
                          {additionalguest.dietaryrestrictions.vegan.value && (
                            <div>
                              <span className="inline-flex rounded-full px-2 text-xs font-semibold leading-5 text-white bg-lime-500 mr-2 mb-2">
                                Vegan
                              </span>
                            </div>
                          )}
                          {!!additionalguest.dietaryrestrictions.others
                            .value && (
                            <div>
                              <span className="inline-flex rounded-full px-2 text-xs font-semibold leading-5 text-white bg-blue-600 truncate mr-2 mb-2">
                                {
                                  additionalguest.dietaryrestrictions.others
                                    .value
                                }
                              </span>
                            </div>
                          )}
                        </td>
                        <td className="px-3 py-3 text-sm align-top">
                          <span className={attendingClasses(guest.isattending)}>
                            {guest.isattending}
                          </span>
                        </td>
                        <td className="py-3 pl-3 pr-4 text-right text-sm font-medium sm:pr-6 align-top">
                          {/* <button className="flex justify-center items-center text-themegreen hover:underline">
                        <PencilAltIcon className="h-5 w-5" />
                        Edit<span className="sr-only">, {guest.name}</span>
                      </button> */}
                        </td>
                      </tr>
                    ))}
                </React.Fragment>
              ))}
            </tbody>
          </table>
        </div>
      </section>
    </React.Fragment>
  );
};

export default GuestList;
