/* This example requires Tailwind CSS v2.0+ */
import { Fragment, useState } from "react";
import { Dialog, Transition } from "@headlessui/react";
import {
  HomeIcon,
  Bars3Icon,
  UsersIcon,
  XMarkIcon,
  ArrowRightOnRectangleIcon,
} from "@heroicons/react/24/outline";
import { NavLink, Outlet } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { loginActions } from "../../../store/login-slice";
import Notification from "../../../shared/UI/Notifications";

const navigation = [
  { name: "Overview", href: "/dashboard", icon: HomeIcon },
  { name: "Guests List", href: "/dashboard/guest-list", icon: UsersIcon },
];

const Dashboard = () => {
  const dispatch = useDispatch();
  const notification = useSelector((state) => state.ui.notification);
  const [sidebarOpen, setSidebarOpen] = useState(false);

  const logoutHandler = (e) => {
    e.preventDefault();
    dispatch(loginActions.logout());
  };

  return (
    <div>
      <Notification
        show={notification.show}
        success={notification.success}
        title={notification.title}
        message={notification.message}
      />
      <Transition.Root show={sidebarOpen} as={Fragment}>
        <Dialog
          as="div"
          className="relative z-40 md:hidden"
          onClose={setSidebarOpen}
        >
          <Transition.Child
            as={Fragment}
            enter="transition-opacity ease-linear duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="transition-opacity ease-linear duration-300"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <div className="fixed inset-0 bg-gray-600 bg-opacity-75" />
          </Transition.Child>

          <div className="fixed inset-0 flex z-40">
            <Transition.Child
              as={Fragment}
              enter="transition ease-in-out duration-300 transform"
              enterFrom="-translate-x-full"
              enterTo="translate-x-0"
              leave="transition ease-in-out duration-300 transform"
              leaveFrom="translate-x-0"
              leaveTo="-translate-x-full"
            >
              <Dialog.Panel className="relative flex-1 flex flex-col max-w-xs w-full bg-themegreen">
                <Transition.Child
                  as={Fragment}
                  enter="ease-in-out duration-300"
                  enterFrom="opacity-0"
                  enterTo="opacity-100"
                  leave="ease-in-out duration-300"
                  leaveFrom="opacity-100"
                  leaveTo="opacity-0"
                >
                  <div className="absolute top-0 right-0 -mr-12 pt-2">
                    <button
                      type="button"
                      className="ml-1 flex items-center justify-center h-10 w-10 rounded-full focus:outline-none focus:ring-2 focus:ring-inset focus:ring-themegreen"
                      onClick={() => setSidebarOpen(false)}
                    >
                      <span className="sr-only">Close sidebar</span>
                      <XMarkIcon
                        className="h-6 w-6 text-white"
                        aria-hidden="true"
                      />
                    </button>
                  </div>
                </Transition.Child>
                <div className="flex-1 h-0 pt-5 pb-4 overflow-y-auto">
                  <nav className="mt-5 px-2 space-y-1">
                    {navigation.map((item) => (
                      <NavLink
                        key={item.name}
                        to={item.href}
                        end
                        className={({ isActive }) =>
                          isActive
                            ? "bg-darkgreen text-white group flex items-center px-2 py-2 text-sm font-medium rounded-md transition-all"
                            : "text-white hover:bg-darkgreen hover:bg-opacity-75 group flex items-center px-2 py-2 text-sm font-medium rounded-md transition-all"
                        }
                      >
                        <item.icon
                          className="mr-4 flex-shrink-0 h-6 w-6 text-white"
                          aria-hidden="true"
                        />
                        {item.name}
                      </NavLink>
                    ))}
                  </nav>
                  <footer className="px-2 py-2 space-y-1 border-t border-white">
                    <button
                      className="w-full text-white hover:bg-darkgreen hover:bg-opacity-75 group flex items-center px-2 py-2 text-sm font-medium rounded-md transition-all"
                      onClick={logoutHandler}
                    >
                      <ArrowRightOnRectangleIcon className="mr-3 flex-shrink-0 h-6 w-6 text-white" />
                      Logout
                    </button>
                  </footer>
                </div>
              </Dialog.Panel>
            </Transition.Child>
            <div className="flex-shrink-0 w-14" aria-hidden="true">
              {/* Force sidebar to shrink to fit close icon */}
            </div>
          </div>
        </Dialog>
      </Transition.Root>

      {/* Static sidebar for desktop */}
      <div className="hidden md:flex md:w-64 md:flex-col md:fixed md:inset-y-0">
        {/* Sidebar component, swap this element with another sidebar if you like */}
        <div className="flex-1 flex flex-col min-h-0 bg-themegreen">
          <div className="flex-1 flex flex-col overflow-y-auto">
            <nav className="mt-5 flex-1 px-2 space-y-1">
              {navigation.map((item) => (
                <NavLink
                  key={item.name}
                  to={item.href}
                  end
                  className={({ isActive }) =>
                    isActive
                      ? "bg-darkgreen text-white group flex items-center px-2 py-2 text-sm font-medium rounded-md transition-all"
                      : "text-white hover:bg-darkgreen hover:bg-opacity-75 group flex items-center px-2 py-2 text-sm font-medium rounded-md transition-all"
                  }
                >
                  <item.icon
                    className="mr-3 flex-shrink-0 h-6 w-6 text-white"
                    aria-hidden="true"
                  />
                  {item.name}
                </NavLink>
              ))}
            </nav>
            <footer className="px-2 py-2 space-y-1 border-t border-white">
              <button
                className="w-full text-white hover:bg-darkgreen hover:bg-opacity-75 group flex items-center px-2 py-2 text-sm font-medium rounded-md transition-all"
                onClick={logoutHandler}
              >
                <ArrowRightOnRectangleIcon className="mr-3 flex-shrink-0 h-6 w-6 text-white" />
                Logout
              </button>
            </footer>
          </div>
        </div>
      </div>
      <div className="md:pl-64 flex flex-col flex-1">
        <div className="sticky top-0 z-10 md:hidden pl-1 pt-1 sm:pl-3 sm:pt-3 bg-gray-100">
          <button
            type="button"
            className="-ml-0.5 -mt-0.5 h-12 w-12 inline-flex items-center justify-center rounded-md text-gray-500 hover:text-gray-900 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-themegreen"
            onClick={() => setSidebarOpen(true)}
          >
            <span className="sr-only">Open sidebar</span>
            <Bars3Icon className="h-6 w-6" aria-hidden="true" />
          </button>
        </div>
        <main className="flex-1">
          <div className="py-6">
            <div className="max-w-7xl mx-auto px-4 sm:px-6 md:px-8">
              <div className="py-4">
                <Outlet />
              </div>
            </div>
          </div>
        </main>
      </div>
    </div>
  );
};

export default Dashboard;
