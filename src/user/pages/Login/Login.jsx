import axios from "axios";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Input from "../../../shared/Form/Input";
import SubmitButton from "../../../shared/Form/SubmitButton";

import { loginActions } from "../../../store/login-slice";
import LoadingSpinner from "../../../shared/UI/LoadingSpinner";
import { uiActions } from "../../../store/ui-slice";

function Login() {
  const dispatch = useDispatch();
  const isLoading = useSelector((state) => state.ui.isLoading);
  // const [isLogin, setIsLogin] = useState(true);

  const [data, setData] = useState({
    userid: "",
    password: "",
  });

  const submitHandler = async (e) => {
    e.preventDefault();

    dispatch(
      uiActions.setIsLoading({
        value: true,
      })
    );

    // if (isLogin) {
    try {
      const response = await axios({
        method: "post",
        headers: {
          "Content-Type": "application/json",
        },
        url: `${process.env.REACT_APP_BACKEND_URL}users/login`,
        data,
      });

      dispatch(
        loginActions.login({
          userId: response.data.userId,
          token: response.data.token,
          expiration: new Date(
            new Date().getTime() + 1000 * 60 * 60
          ).toISOString(),
        })
      );
    } catch (error) {
      console.log(error);
    }
    // } else {
    //   try {
    //     const response = await axios({
    //       method: "post",
    //       headers: {
    //         "Content-Type": "application/json",
    //       },
    //       url: `${process.env.REACT_APP_BACKEND_URL}users/signup`,
    //       data,
    //     });

    //     dispatch(
    //       loginActions.login({
    //         userId: response.data.userId,
    //         token: response.data.token,
    //         expiration: new Date(
    //           new Date().getTime() + 1000 * 60 * 60
    //         ).toISOString(),
    //       })
    //     );
    //   } catch (error) {
    //     console.log(error);
    //   }
    // }

    dispatch(
      uiActions.setIsLoading({
        value: false,
      })
    );
  };

  const inputChangeHandler = (e) => {
    const currentData = { ...data };
    currentData[e.target.name] = e.target.value;
    setData(currentData);
  };

  // const toggleSignUpAndLogin = (e) => {
  //   e.preventDefault();

  //   setIsLogin((prevState) => !prevState);
  // };

  return (
    <React.Fragment>
      {isLoading ? (
        <LoadingSpinner text="Please wait..."></LoadingSpinner>
      ) : (
        <div className="px-5 md:px-0 relative w-screen h-screen">
          <form
            className="w-[calc(100%_-_40px)] max-w-[480px] absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 border border-solid border-[#ced4da] bg-white px-6 py-8 rounded-lg"
            onSubmit={submitHandler}
          >
            <header>
              {/* <h1 className="text-3xl font-medium text-gray-800 mb-2">{isLogin ? "Login" : "Sign up"}</h1> */}
              <h1 className="font-body text-3xl font-medium text-gray-800 mb-2 text-center">
                Login
              </h1>
            </header>
            <section>
              <Input
                inputLabel="User ID"
                inputName="userid"
                placeholder="Enter User ID"
                inputChangeHandler={inputChangeHandler}
                autoComplete="on"
                isRequired="false"
              ></Input>
              <Input
                inputLabel="Password"
                inputName="password"
                inputType="password"
                inputChangeHandler={inputChangeHandler}
                placeholder="Enter your password"
                autoComplete="on"
                isRequired="false"
              ></Input>
              <footer className="login-form__footer">
                {/* <SubmitButton
                  className="w-full"
                  textLabel={isLogin ? "Login" : "Sign up"}
                ></SubmitButton> */}
                <SubmitButton
                  className="w-full"
                  textLabel="Login"
                ></SubmitButton>
                {/* <p className="mt-4 text-center">
                  {isLogin
                    ? "Don't have a User ID yet?"
                    : "Have a User ID already?"}
                  <button
                    className="text-darkgreen ml-2 hover:underline"
                    onClick={toggleSignUpAndLogin}
                  >
                    {isLogin ? "Sign up" : "Login"}
                  </button>
                </p> */}
              </footer>
            </section>
          </form>
        </div>
      )}
    </React.Fragment>
  );
}

export default Login;
