import React from "react";
import Radio from "../../shared/Form/Radio";

import { useDispatch, useSelector } from "react-redux";
import { guestActions } from "../../store/guest-slice";
import FormLabel from "../../shared/Form/Label";

function WelcomeForm(props) {
  const dispatch = useDispatch();
  const guestData = useSelector((state) => state.guest.guestData);

  const inputChangeHandler = (e) => {
    dispatch(
      guestActions.mainGuestPropertyChange({
        property: e.target.name,
        value: e.target.value,
      })
    );
  };

  return (
    <section className="mb-4 px-6 py-8 rounded-lg border border-solid border-[#ced4da] bg-white relative">
      <h2 className="font-body font-medium text-2xl md:text-4xl mb-2 text-gray-800">
        Hello{guestData.name ? ` ${guestData.name}` : ""}!
      </h2>
      <p className="text-sm mb-2">
        We are delighted to celebrate our special day with you!
      </p>
      <div>
        <FormLabel>Will you be able to attend our wedding banquet?</FormLabel>
        <div>
          <Radio
            inline
            radioName="isattending"
            radioLabel="Yes"
            radioValue="Yes"
            radioID="isattending1"
            changeHandler={inputChangeHandler}
            isChecked={guestData.isattending === "Yes"}
          ></Radio>
          <Radio
            inline
            radioName="isattending"
            radioLabel="No"
            radioValue="No"
            radioID="isattending2"
            changeHandler={inputChangeHandler}
            isChecked={guestData.isattending === "No"}
          ></Radio>
        </div>
      </div>
    </section>
  );
}

export default WelcomeForm;
