import React from "react";
import Input from "../../shared/Form/Input";
import Checkbox from "../../shared/Form/Checkbox";
import RemoveGuest from "./RemoveGuest";

import { useDispatch } from "react-redux";
import { guestActions } from "../../store/guest-slice";
import FormLabel from "../../shared/Form/Label";

function FormPerPerson(props) {
  const dispatch = useDispatch();

  const nameChangeHandler = (e) => {
    dispatch(
      guestActions.additionalGuestPropertyChange({
        i: props.guestIndex,
        value: e.target.value,
        property: "name",
      })
    );
  };

  const dietaryRestrictionsChangeHandler = (e) => {
    dispatch(
      guestActions.additionalGuestDietaryRestrictionsChange({
        i: props.guestIndex,
        value: e.target.checked,
        name: e.target.name,
      })
    );
  };

  const toggleOthersField = (e) => {
    dispatch(
      guestActions.additionalGuestDietaryRestrictionsChange({
        i: props.guestIndex,
        value: "",
        name: "others",
      })
    );

    dispatch(
      guestActions.additionalGuestToggleOthers({
        i: props.guestIndex,
      })
    );
  };

  const otherRestrictionsChangeHandler = (e) => {
    dispatch(
      guestActions.additionalGuestDietaryRestrictionsChange({
        i: props.guestIndex,
        value: e.target.value,
        name: "others",
      })
    );
  };

  return (
    <div className="mb-4 px-6 py-8 rounded-lg border border-solid border-[#ced4da] bg-white relative last:m-0">
      <RemoveGuest guestId={props.guestId}></RemoveGuest>
      <Input
        inputLabel="Name"
        inputName="name"
        inputChangeHandler={nameChangeHandler}
        placeholder="Enter your name"
        value={props.guest.name}
        isRequired="true"
      ></Input>
      <div>
        <FormLabel>Do you have any dietary restrictions?</FormLabel>
        <div>
          <Checkbox
            inline
            checkboxName="halal"
            checkboxLabel="Halal"
            checkboxID={`dietaryRestrictions1-${props.guestIndex + 1}`}
            changeHandler={dietaryRestrictionsChangeHandler}
            isChecked={props.guest.dietaryrestrictions.halal.value}
          ></Checkbox>
          <Checkbox
            inline
            checkboxName="vegan"
            checkboxLabel="Vegan"
            checkboxID={`dietaryRestrictions2-${props.guestIndex + 1}`}
            changeHandler={dietaryRestrictionsChangeHandler}
            isChecked={props.guest.dietaryrestrictions.vegan.value}
          ></Checkbox>
          <Checkbox
            inline
            checkboxName="nobeef"
            checkboxLabel="No beef"
            checkboxID={`dietaryRestrictions3-${props.guestIndex + 1}`}
            changeHandler={dietaryRestrictionsChangeHandler}
            isChecked={props.guest.dietaryrestrictions.nobeef.value}
          ></Checkbox>
          <Checkbox
            checkboxName="others"
            checkboxLabel="Others:"
            checkboxID={`dietaryRestrictions4-${props.guestIndex + 1}`}
            changeHandler={toggleOthersField}
            isChecked={props.guest.dietaryrestrictions.others.enabled}
          ></Checkbox>
          <input
            type="text"
            className="focus:ring-themegreen focus:border-themegreen block w-full text-sm border-gray-300 rounded disabled:bg-slate-200"
            onChange={otherRestrictionsChangeHandler}
            placeholder="E.g. Allergic to peanut and seafood"
            value={props.guest.dietaryrestrictions.others.value}
            disabled={!props.guest.dietaryrestrictions.others.enabled}
          />
        </div>
      </div>
    </div>
  );
}

export default FormPerPerson;
