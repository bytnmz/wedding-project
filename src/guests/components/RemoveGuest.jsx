import { TrashIcon } from "@heroicons/react/24/solid";
import React from "react";
import { useDispatch } from "react-redux";
import { guestActions } from "../../store/guest-slice";

function RemoveGuest(props) {
  const dispatch = useDispatch();

  const removeGuestHandler = (e) => {
    e.preventDefault();

    dispatch(
      guestActions.removeGuest({
        guestId: props.guestId,
      })
    );
  };

  return (
    <button
      className="absolute top-5 right-6 flex justify-center items-center w-8 h-8 rounded bg-error p-0 text-white focus:shadow-[0_0_0_4px_rgba(225,83,97,0.5)] focus:border-error"
      aria-label="Remove guest"
      onClick={removeGuestHandler}
    >
      <TrashIcon className="h-4 w-4" />
    </button>
  );
}

export default RemoveGuest;
