import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import Input from "../../shared/Form/Input";
import SubmitButton from "../../shared/Form/SubmitButton";

import { guestActions } from "../../store/guest-slice";

function GetGuestId() {
  const dispatch = useDispatch();
  const guestId = useSelector((state) => state.guest.guestId);
  const fetchingError = useSelector((state) => state.guest.fetchingError);
  const navigate = useNavigate();

  const inputChangeHandler = (e) => {
    dispatch(
      guestActions.guestIdChange({
        value: e.target.value,
      })
    );
  };

  const submitHandler = (e) => {
    e.preventDefault();
    // navigate(`/rsvp?guestId=${guestId}`, { replace: true });
    navigate(`/rsvp?guestId=${guestId}`, { replace: false });
  };

  return (
    <div className="px-5 md:px-0 mx-auto my-10 max-w-[720px]">
      <form
        className="px-6 py-8 rounded-lg border border-solid border-[#ced4da] bg-white relative"
        onSubmit={submitHandler}
      >
        <h2 className="font-body font-medium text-2xl md:text-4xl mb-2 md:mb-4 text-gray-800">
          {!fetchingError ? "Sorry!" : "Oops!"}
        </h2>
        {fetchingError && (
          <React.Fragment>
            <p className="mb-2 text-sm">
              We are unable to retrieve your guest entry from our guest list.{" "}
              <br></br> Please provide us your assigned guest ID below and we
              will retrieve your entry for you.
            </p>
            <p className="mb-2 text-sm">
              Alternatively, access your form with the link we have given you in
              our invitation message.
            </p>
          </React.Fragment>
        )}
        {!fetchingError && (
          <p className="mb-2 text-sm">
            The Guest ID provided is incorrect. Please double check your entry!
          </p>
        )}
        <section className="guest-id-form__body">
          <Input
            inputLabel="Guest ID"
            inputName="guestid"
            inputChangeHandler={inputChangeHandler}
            placeholder="Enter your guest ID"
            value={guestId || ""}
            isRequired="true"
          ></Input>
          <footer>
            <SubmitButton
              className="w-full"
              textLabel="Retrieve"
            ></SubmitButton>
          </footer>
        </section>
      </form>
    </div>
  );
}

export default GetGuestId;
