import { UserPlusIcon } from "@heroicons/react/24/solid";
import React from "react";
import { useDispatch } from "react-redux";
import { guestActions } from "../../store/guest-slice";

function AddNewGuestButton(props) {
  const dispatch = useDispatch();

  const clickHandler = () => {
    dispatch(guestActions.addNewGuest());
  };

  return (
    <button
      className="flex justify-center items-center rounded bg-themegreen px-4 py-2 text-white text-sm border border-solid border-themegreen hover:bg-darkgreen focus:shadow-[0_0_0_4px_rgba(10,186,181,0.5)] transition-all"
      onClick={clickHandler}
    >
      <UserPlusIcon className="h-6 w-6 mr-2" />
      Add Guest
    </button>
  );
}

export default AddNewGuestButton;
