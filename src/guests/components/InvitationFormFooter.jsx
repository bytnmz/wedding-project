import React from "react";
import InvitationFormSummary from "./InvitationFormSummary";
import AddNewGuestButton from "./AddNewGuestButton";
import SubmitButton from "../../shared/Form/SubmitButton";

import { useSelector } from "react-redux";

function InvitationFormFooter() {
  const guestData = useSelector((state) => state.guest.guestData);

  return (
    <footer>
      {guestData.isattending === "Yes" && (
        <InvitationFormSummary></InvitationFormSummary>
      )}
      <div className="flex justify-between flex-row-reverse">
        <SubmitButton textLabel="Send"></SubmitButton>
        {guestData.isattending === "Yes" && (
          <AddNewGuestButton></AddNewGuestButton>
        )}
      </div>
    </footer>
  );
}

export default InvitationFormFooter;
