import React from "react";
import ImagePlaceholder from "../../shared/UI/ImagePlaceholder";

function InvitationFormBanner() {
  return (
    <div className="rounded-lg mb-4 overflow-hidden">
      <ImagePlaceholder className="m-0 relative pt-[40%] sm:pt-[33.333%]">
        <img
          className="object-cover absolute w-full h-full top-0 left-0 right-0 bottom-0"
          src={`${process.env.REACT_APP_IMAGE_ASSET_URL}banner-photo.webp`}
          alt=""
          loading="lazy"
        />
      </ImagePlaceholder>
    </div>
  );
}

export default InvitationFormBanner;
