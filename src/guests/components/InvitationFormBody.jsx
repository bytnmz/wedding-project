import React from "react";
import MainGuest from "./MainGuest";
import FormPerPerson from "./FormPerPerson";

import { useSelector } from "react-redux";

function InvitationFormBody() {
  const guestData = useSelector((state) => state.guest.guestData);

  return (
    <div className="mb-4">
      <MainGuest></MainGuest>
      {guestData.additionalGuests.map((guest, index) => {
        const id = guest._id || guest.id;
        return (
          <FormPerPerson
            key={id}
            guestIndex={index}
            guestId={id}
            guest={guest}
          ></FormPerPerson>
        );
      })}
    </div>
  );
}

export default InvitationFormBody;
