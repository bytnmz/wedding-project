import React from "react";
import { useSelector } from "react-redux";

function InvitationFormSummary() {
  const guestData = useSelector((state) => state.guest.guestData);

  const getTotalPax = () => {
    return (
      1 +
      guestData.additionalGuests.filter((guest) => guest.name.length > 0).length
    );
  };

  return (
    guestData.name.length > 0 && (
      // <div className="invitation-form-summary">
      <div className="mb-4 px-6 py-8 bg-white relative rounded-lg border border-solid border-[#dedede]">
        <h2 className="font-body pb-2 font-semibold text-md text-gray-800">
          Summary
        </h2>
        <ol className="mb-4 px-0 py-0 text-sm">
          <li className="flex justify-between">
            <span>Guest #1: </span>
            <span>{guestData.name}</span>
          </li>
          {guestData.additionalGuests.map((invitee, index) => {
            return (
              invitee.name.length > 0 && (
                <li key={invitee.name} className="flex justify-between">
                  <span>Guest #{index + 2}: </span>
                  <span>{invitee.name}</span>
                </li>
              )
            );
          })}
        </ol>
        <footer className="pt-2 mt-4 text-right text-sm font-semibold border-t border-solid border-[#dedede] text-gray-800">
          <span>Total pax: {getTotalPax()}</span>
        </footer>
      </div>
    )
  );
}

export default InvitationFormSummary;
