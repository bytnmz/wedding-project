import React from "react";
import Input from "../../shared/Form/Input";
import Checkbox from "../../shared/Form/Checkbox";
import Radio from "../../shared/Form/Radio";
import { useDispatch, useSelector } from "react-redux";
import { guestActions } from "../../store/guest-slice";
import FormLabel from "../../shared/Form/Label";

function MainGuest() {
  const dispatch = useDispatch();
  const guestData = useSelector((state) => state.guest.guestData);

  const inputChangeHandler = (e) => {
    dispatch(
      guestActions.mainGuestPropertyChange({
        property: e.target.name,
        value: e.target.value,
      })
    );
  };

  const dietaryRestrictionsChangeHandler = (e) => {
    dispatch(
      guestActions.mainGuestDietaryRestrictionsChange({
        value: e.target.checked,
        name: e.target.name,
      })
    );
  };

  const toggleOthersField = (e) => {
    dispatch(
      guestActions.mainGuestDietaryRestrictionsChange({
        value: "",
        name: "others",
      })
    );
    dispatch(guestActions.mainGuestToggleOthers());
  };

  const otherRestrictionsChangeHandler = (e) => {
    dispatch(
      guestActions.mainGuestDietaryRestrictionsChange({
        value: e.target.value,
        name: "others",
      })
    );
  };

  return (
    <div className="mb-4 px-6 py-8 rounded-lg border border-solid border-[#ced4da] bg-white relative last:m-0">
      <Input
        inputLabel="Name"
        inputName="name"
        inputChangeHandler={inputChangeHandler}
        placeholder="Enter your name"
        value={guestData.name}
        isRequired="true"
      ></Input>
      <Input
        inputLabel="Contact Number"
        inputName="contact"
        inputType="tel"
        inputChangeHandler={inputChangeHandler}
        isRequired="true"
        placeholder="Enter contact number"
        value={guestData.contact}
      ></Input>
      <Input
        inputLabel="Email Address"
        inputName="email"
        inputType="email"
        inputChangeHandler={inputChangeHandler}
        isRequired="true"
        placeholder="name@example.com"
        value={guestData.email}
        describeById="emailDesc"
        describeByText="We will send you updates via email. Don't worry, we won't spam you!"
      ></Input>
      <Input
        inputLabel="Mailing Address"
        inputName="address"
        inputChangeHandler={inputChangeHandler}
        placeholder="1 Orchard Road, Singapore 000001"
        value={guestData.address}
        describeById="mailingDesc"
        describeByText="We will mail our invitation card to you if provided."
      ></Input>
      <div className="mb-4">
        <FormLabel>Do you have any dietary restrictions?</FormLabel>
        <div>
          <Checkbox
            inline
            checkboxName="halal"
            checkboxLabel="Halal"
            checkboxID={`dietaryRestrictions1-0`}
            changeHandler={dietaryRestrictionsChangeHandler}
            isChecked={guestData.dietaryrestrictions.halal.value}
          ></Checkbox>
          <Checkbox
            inline
            checkboxName="vegan"
            checkboxLabel="Vegan"
            checkboxID={`dietaryRestrictions2-0`}
            changeHandler={dietaryRestrictionsChangeHandler}
            isChecked={guestData.dietaryrestrictions.vegan.value}
          ></Checkbox>
          <Checkbox
            inline
            checkboxName="nobeef"
            checkboxLabel="No beef"
            checkboxID={`dietaryRestrictions3-0`}
            changeHandler={dietaryRestrictionsChangeHandler}
            isChecked={guestData.dietaryrestrictions.nobeef.value}
          ></Checkbox>
          <Checkbox
            checkboxName="others"
            checkboxLabel="Others:"
            checkboxID={`dietaryRestrictions4-0`}
            changeHandler={toggleOthersField}
            isChecked={guestData.dietaryrestrictions.others.enabled}
          ></Checkbox>
          <input
            type="text"
            className="focus:ring-themegreen focus:border-themegreen block w-full text-sm border-gray-300 rounded disabled:bg-slate-200"
            onChange={otherRestrictionsChangeHandler}
            placeholder="E.g. Allergic to peanut and seafood"
            value={guestData.dietaryrestrictions.others.value || ""}
            disabled={!guestData.dietaryrestrictions.others.enabled}
            required
          />
        </div>
      </div>
      <div className="mb-4">
        <FormLabel>Do you need a parking coupon?</FormLabel>
        <div>
          <Radio
            inline
            radioName="parkingcoupon"
            radioLabel="Yes"
            radioValue="Yes"
            radioID="parkingcoupon1"
            changeHandler={inputChangeHandler}
            isChecked={guestData.parkingcoupon === "Yes"}
          ></Radio>
          <Radio
            inline
            radioName="parkingcoupon"
            radioLabel="No"
            radioValue="No"
            radioID="parkingcoupon2"
            changeHandler={inputChangeHandler}
            isChecked={guestData.parkingcoupon === "No"}
          ></Radio>
        </div>
      </div>
      <Input
        inputLabel="Special Requirements"
        inputName="requirements"
        inputChangeHandler={inputChangeHandler}
        placeholder="E.g. 2 baby chairs"
        value={guestData.requirements}
      ></Input>
    </div>
  );
}

export default MainGuest;
