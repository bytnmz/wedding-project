import { DevicePhoneMobileIcon } from "@heroicons/react/24/outline";
import { CalendarIcon, MapPinIcon } from "@heroicons/react/24/solid";
import React from "react";

function InvitationFormHeader() {
  return (
    <header className="mb-4 p-6 bg-white border border-solid border-[#ced4da] rounded-lg">
      <h1 className="font-body font-medium text-2xl md:text-4xl mb-2 text-gray-800">
        Bayu & Wenjin's Wedding Banquet
      </h1>
      <p className="mb-1 flex items-start text-sm">
        <CalendarIcon className="h-4 w-4 mt-[2px] mr-2 grow-0 shrink-0 text-themegreen" />
        30 Oct 2022, 18:30 - 23:00
      </p>
      <p className="mb-1 flex items-start text-sm">
        <MapPinIcon className="h-4 w-4 mt-[2px] mr-2 grow-0 shrink-0 text-themegreen" />
        <a
          className="inline-block text-link underline"
          href="https://goo.gl/maps/vJbEGTTiQg24UG5P6"
          target="_blank"
          rel="noreferrer"
        >
          The Glasshouse @ Andaz Hotel, 5 Fraser St, Singapore 189354
        </a>
      </p>
      <p className="flex items-start text-sm">
        <DevicePhoneMobileIcon className="h-4 w-4 mt-[2px] mr-2 grow-0 shrink-0 text-themegreen" />
        <div>
          <span>Bayu: </span>
          <a
            className="inline-block text-link underline mx-1"
            href="tel:96748511"
          >
            96748511
          </a>{" "}
          <span>| Wenjin: </span>
          <a
            className="inline-block text-link underline mx-1"
            href="tel:83587226"
          >
            83587226
          </a>
        </div>
      </p>
    </header>
  );
}

export default InvitationFormHeader;
