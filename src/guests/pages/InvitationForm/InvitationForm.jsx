import axios from "axios";
import React, { useCallback, useEffect } from "react";
import { useNavigate, useLocation } from "react-router-dom";

import InvitationFormBanner from "../../components/InvitationFormBanner";
import InvitationFormHeader from "../../components/InvitationFormHeader";
import InvitationFormBody from "../../components/InvitationFormBody";
import InvitationFormFooter from "../../components/InvitationFormFooter";
import GetGuestId from "../../components/GetGuestId";

import WelcomeForm from "../../components/WelcomeForm";

import { useDispatch, useSelector } from "react-redux";
import { uiActions } from "../../../store/ui-slice";
import { guestActions } from "../../../store/guest-slice";
import LoadingSpinner from "../../../shared/UI/LoadingSpinner";

function InvitationForm() {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const location = useLocation();
  const isLoading = useSelector((state) => state.ui.isLoading);
  const hasGuestId = useSelector((state) => state.guest.hasGuestId);
  const guestId = useSelector((state) => state.guest.guestId);
  const guestData = useSelector((state) => state.guest.guestData);
  const guestDataHasChanged = useSelector(
    (state) => state.guest.guestDataHasChanged
  );

  useEffect(() => {
    window.onpopstate = (e) => {
      if (guestDataHasChanged) {
        if (
          window.confirm(
            "Are you sure you want to leave the page? You will lose your changes."
          )
        ) {
          dispatch(guestActions.resetGuestData());
        } else {
          window.history.pushState(null, null, `/rsvp?guestId=${guestId}`);
        }
      } else {
        dispatch(guestActions.resetGuestData());
      }
    };
  }, [guestId, guestDataHasChanged, dispatch]);

  const getGuestData = useCallback(
    async (id) => {
      dispatch(
        uiActions.setIsLoading({
          value: true,
        })
      );

      const data = {
        id,
      };

      try {
        const response = await axios({
          method: "post",
          url: `${process.env.REACT_APP_BACKEND_URL}guests/get`,
          data,
        });

        dispatch(
          guestActions.setGuestData({
            guest: response.data.guest,
            guestId: id,
          })
        );
      } catch (error) {
        console.log(error);
        dispatch(guestActions.setFetchError());
      }

      dispatch(
        uiActions.setIsLoading({
          value: false,
        })
      );
    },
    [dispatch]
  );

  useEffect(() => {
    if (location.search) {
      const queryParams = new URLSearchParams(location.search);

      if (queryParams.has("guestId")) {
        getGuestData(queryParams.get("guestId"));
      }
    }
  }, [location, getGuestData]);

  const updateGuestData = async (e) => {
    e.preventDefault();

    dispatch(
      uiActions.setIsLoading({
        value: true,
      })
    );

    dispatch(
      guestActions.setGuestDataHasChanged({
        value: false,
      })
    );

    try {
      const response = await axios({
        method: "patch",
        url: `${process.env.REACT_APP_BACKEND_URL}guests/update`,
        data: guestData,
      });

      dispatch(
        guestActions.updateGuestData({
          status: response.status,
        })
      );

      // navigate(`/feedback`, { replace: true });
      navigate("/feedback", { replace: true });
    } catch (error) {
      dispatch(
        guestActions.updateGuestData({
          status: error.response.status,
        })
      );

      navigate("/feedback", { replace: true });
    }
  };

  return (
    <React.Fragment>
      {isLoading ? (
        <LoadingSpinner text="Loading..."></LoadingSpinner>
      ) : (
        <React.Fragment>
          {!hasGuestId && <GetGuestId />}
          {hasGuestId && (
            <form
              className="px-5 md:px-0 mx-auto my-10 max-w-[720px]"
              onSubmit={updateGuestData}
            >
              <InvitationFormBanner />
              <InvitationFormHeader />
              <WelcomeForm />
              {guestData.isattending === "Yes" && <InvitationFormBody />}
              <InvitationFormFooter />
            </form>
          )}
        </React.Fragment>
      )}
    </React.Fragment>
  );
}

export default InvitationForm;
