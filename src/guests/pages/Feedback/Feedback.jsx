import { CheckCircleIcon, XCircleIcon } from "@heroicons/react/24/solid";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { guestActions } from "../../../store/guest-slice";
import { uiActions } from "../../../store/ui-slice";

function Feedback() {
  const dispatch = useDispatch();
  const updateSuccess = useSelector((state) => state.guest.updateSuccess);

  useEffect(() => {
    dispatch(
      uiActions.setIsLoading({
        value: false,
      })
    );

    dispatch(guestActions.resetGuestData());
  }, [dispatch]);

  return (
    <React.Fragment>
      {updateSuccess ? (
        <div className="px-5 md:px-0 mx-auto my-10 max-w-[480px]">
          <section className="px-12 py-8 rounded-lg border border-solid border-[#ced4da] bg-white relative text-center">
            <CheckCircleIcon className="inline-block mb-4 w-16 text-themegreen" />
            <h1 className="font-body font-medium text-4xl mb-6 text-gray-800">
              Thank you!
            </h1>
            <p className="mb-3 text-sm">Your response has been received!</p>
            <p className="text-sm">
              If you wish to update your response, you can use the same link
              that we have provided you in our invitation message.
            </p>
          </section>
        </div>
      ) : (
        <div className="px-5 md:px-0 mx-auto my-10 max-w-[480px]">
          <section className="px-12 py-8 rounded-lg border border-solid border-[#ced4da] bg-white relative text-center">
            <XCircleIcon className="inline-block mb-4 w-16 text-error" />
            <h1 className="font-body font-medium text-4xl mb-6 text-gray-800">
              Sorry!
            </h1>
            <p className="text-sm">
              There was an error processing your request! Please try again later
              or let us know if the issue persists.
            </p>
          </section>
        </div>
      )}
    </React.Fragment>
  );
}

export default Feedback;
