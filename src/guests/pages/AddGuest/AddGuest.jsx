import React, { useState } from "react";
import { useSelector } from "react-redux";
import axios from "axios";

import Input from "../../../shared/Form/Input";
import SubmitButton from "../../../shared/Form/SubmitButton";

const AddGuest = () => {
  const token = useSelector((state) => state.login.token);
  const [data, setData] = useState({
    isattending: "Yes",
    name: "",
    contact: "",
    email: "",
    address: "",
    dietaryrestrictions: {
      vegan: {
        value: false,
      },
      halal: {
        value: false,
      },
      nobeef: {
        value: false,
      },
      others: {
        enabled: false,
        value: "",
      },
    },
    requirements: "",
    parkingcoupon: "No",
    additionalGuests: [],
  });

  const inputChangeHandler = (e) => {
    const currentData = { ...data };
    currentData[e.target.name] = e.target.value;
    setData(currentData);
  };

  const addGuest = async (e) => {
    e.preventDefault();

    try {
      const response = await axios({
        method: "post",
        url: `${process.env.REACT_APP_BACKEND_URL}guests/add`,
        headers: {
          Authorization: `Bearer ${token}`,
        },
        data,
      });
      console.log(response);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <form onSubmit={addGuest}>
      <section className="px-6 py-8 rounded-lg border border-solid border-slate-300 bg-white relative">
        <Input
          inputLabel="Guest Name"
          inputName="name"
          placeholder="Enter guest's name"
          inputChangeHandler={inputChangeHandler}
          autoComplete="off"
          isRequired="true"
        ></Input>
        <Input
          inputLabel="Contact Number"
          inputName="contact"
          inputType="tel"
          inputChangeHandler={inputChangeHandler}
          placeholder="Enter guest's contact number"
          autoComplete="off"
        ></Input>
        <footer className="flex justify-end">
          <SubmitButton textLabel="Add Guest"></SubmitButton>
        </footer>
      </section>
    </form>
  );
};

export default AddGuest;
